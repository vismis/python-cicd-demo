from app import app

@app.route('/')
def index():
    return 'Hello, World!'

def test_index():
    client = app.test_client()
    response = client.get('/')
    assert response.data == b'Hello, World!'
    assert response.status_code == 200
