
![STATUS](https://gitlab.com/vismis/python-flask-pytest-cicd-demo/badges/main/pipeline.svg)

# Python Flask Project with pytest and CI/CD Demo

Welcome to the Python Flask project demonstration with pytest and CI/CD integration. This repository provides a simple Flask application setup along with tests using pytest, designed to showcase continuous integration and deployment (CI/CD) practices.

## Prerequisites

Before you begin, ensure you have the following installed:

- Python (3.9+ recommended)
- pip (Python package installer)
- virtualenv (optional, for creating virtual environments)

## Getting Started

1. **Clone the repository:**

   ```bash
   git clone <repository_url>
   cd python-flask-pytest-cicd-demo
   ```

2. **Set up virtual environment (optional but recommended):**

   ```bash
   python -m venv venv
   source venv/bin/activate   # On Windows use `venv\Scripts\activate`
   ```

3. **Install dependencies:**

   ```bash
   pip install -r requirements.txt
   ```

## Running the Flask Application

To run the Flask application locally:

```bash
python run.py
```

The application will start on `http://localhost:5000`.

## Running Tests with pytest

To run pytest tests:

```bash
python -m pytest --junitxml=pytest_results.xml tests/test_routes.py
```

This command will execute the tests defined in the `tests/` directory using pytest.

## CI/CD Integration

This project includes a `.gitlab-ci.yml` configuration for GitLab CI/CD. The CI pipeline is set up to:

- Create a virtual environment
- Install dependencies
- Run pytest tests and generate JUnit XML reports
- Archive test reports and other artifacts

## Project Structure

The project structure is as follows:

```
python-flask-pytest-cicd-demo/
├── app/
│   ├── __init__.py
│   ├── routes.py
├── tests/
│   ├── __init__.py
│   ├── test_routes.py
├── venv/
├── config.py
├── requirements.txt
└── run.py
```

- **`app/`**: Contains the main Flask application.
- **`tests/`**: Contains pytest test cases.
- **`venv/`**: Virtual environment directory.
- **`config.py`**: Configuration file (optional).
- **`requirements.txt`**: List of dependencies.
- **`run.py`**: Entry point for the Flask application.

## Additional Information

- Replace `<repository_url>` with the actual URL of your Git repository.
- Customize routes and tests (`app/routes.py` and `tests/test_routes.py`) as per your project requirements.
- Ensure all necessary environment variables, configurations, and permissions are set up as needed.

## Contributing

Feel free to contribute to this project by forking it and submitting a pull request. Any improvements or suggestions are welcome!

## License

This project is licensed under the [MIT License](LICENSE).